import { Draw } from './draw.js';
import { Paddle } from './gameElements/paddle.js';
import { keyDownHandler, keyUpHandler } from './events.js'
import { Ball } from './gameElements/ball.js';
import { Level1 } from './level/level1.js';
import { SparkList } from './gameElements/sparkList.js';

/** @type {HTMLCanvasElement} */
const canvas = document.getElementById('myCanvas');
/** @type {HTMLButtonElement} */
const startButton = document.getElementById('startGame');
globalThis.timer = 10;
globalThis.score = 0;

let ball, paddle, drawer;

async function startGame() {
    globalThis.score = 0;
    canvas.setAttribute('tabindex','0');
    canvas.focus();
    let sparkList = new SparkList();
    const level1 = new Level1(canvas);
    ball = new Ball(canvas, sparkList);
    paddle = new Paddle(canvas);
    drawer = new Draw(canvas, paddle, ball, level1, sparkList);
    startButton.disabled = true;
    document.addEventListener('keydown', event => keyDownHandler(event, paddle));
    document.addEventListener('keyup', event => keyUpHandler(event, paddle));

    await drawer.countDown();
    globalThis.interval = setInterval(() => drawer.draw(), globalThis.timer);
}

globalThis.gameOver = function () {
    clearInterval(globalThis.interval);
    drawer.gameOver();
    startButton.disabled = false;
}

globalThis.win = function () {
    clearInterval(globalThis.interval);
    drawer.win();
    startButton.disabled = false;
}
startButton.addEventListener('click', startGame);
export class Paddle {

    constructor(canvas) {
        this.canvas = canvas;
        this.height = 15;
        this.width = 120;
        this.x = (this.canvas.width - this.width) / 2;
        this.leftPressed = false;
        this.rightPressed = false;
        this.color = 'deepskyblue';
    }

    move() {
        if (this.rightPressed) {
            this.x += 7;
            if (this.x + this.width > this.canvas.width) {
                this.x = this.canvas.width - this.width;
            }
        }
        else if (this.leftPressed) {
            this.x -= 7;
            if (this.x < 0) {
                this.x = 0;
            }
        }
    }

    draw(ctx) {
        ctx.beginPath();
        ctx.rect(this.x, this.canvas.height - this.height, this.width, this.height);
        ctx.fillStyle = this.color;
        ctx.fill();
        ctx.closePath();
    }
}
export class Brick {
    static height = 20;
    static width = 80;

    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.color = 'violet';
        this.show = false;
    }

    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     */
    draw(ctx) {
        ctx.beginPath();
        ctx.rect(this.x, this.y, Brick.width, Brick.height);
        ctx.fillStyle = this.color;
        ctx.fill();
        ctx.stroke();
        ctx.closePath();
    }
}
import { Brick } from "./brick.js";
import { Level } from "./level.js";
import { Paddle } from "./paddle.js";
import { Spark } from "./spark.js";
import { SparkList } from "./sparkList.js";

export class Ball {

    constructor(canvas, sparkList) {
        /** @type {HTMLCanvasElement} */
        this.canvas = canvas;
        /** @type {SparkList} */
        this.sparkList = sparkList;
        this.x = this.canvas.width / 2;
        this.y = this.canvas.height - 30;
        this.dx = 2;
        this.dy = -2;
        this.color = 'white';
        this.radius = 10;
        this.form = Math.PI * 2;
    }

    /**
     * Next horizontal move
     * @param {Level} brickList 
     */
    nextDx(brickList) {
        if (this.x + this.dx > this.canvas.width - this.radius || this.x + this.dx < this.radius || this.isOnBrick(brickList, this.x + this.dx, this.y)) {
            this.dx = -this.dx;
        }
    }

    /**
     * Next vertical move
     * @param {Paddle} paddle 
     * @param {Level} brickList 
     */
    nextDy(paddle, brickList) {
        if (this.y + this.dy > this.canvas.height - this.radius) {
            gameOver();
        } else if (this.isOnPaddle(paddle) || this.y + this.dy < this.radius || this.isOnBrick(brickList, this.x, this.y + this.dy)) {
            this.dy = -this.dy;
        }
    }

    /**
     * Move the ball in the game area
     * @param {Paddle} paddle
     * @param {Level} brickList
     */
    move(paddle, brickList) {
        this.nextDx(brickList);
        this.nextDy(paddle, brickList);
        this.x += this.dx;
        this.y += this.dy;
    }

    isOnPaddle(paddle) {
        return (this.x > paddle.x && this.x < paddle.x + paddle.width + this.radius) && (this.y + this.dy > this.canvas.height - paddle.height - this.radius);
    }

    /**
     * Do ball collide with a brick on x axis
     * @param {Level} Level 
     */
    isOnBrick(brickList, checkX, checkY) {
        let bool;
        let nbAlive = 0;
        brickList.bricks.forEach(row => {
            row.forEach(brick => {
                if (brick.show) {
                    nbAlive++;
                    if ((checkY > brick.y - this.radius && checkY < brick.y + Brick.height + this.radius) && (checkX > brick.x - this.radius && checkX < brick.x + Brick.width + this.radius)) {
                        brick.show = false;
                        bool = true;
                        globalThis.score += 10;
                        this.sparkList.sparks.push(new Spark(brick.x, brick.y));
                    }
                }
            })
        });
        if (nbAlive == 0) {
            win();
        }
        return bool;
    }

    draw(ctx) {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, this.form);
        ctx.fillStyle = this.color;
        ctx.fill();
        ctx.closePath();
    }
}
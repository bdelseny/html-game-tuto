export class Spark {
    static spritePath = `${baseUrl}assets/spark.drawio.png`;
    static spriteWidth = 1280;
    static spriteHeight = 160;
    static frameWidth = 160;
    static frameHeight = 160;
    static maxFrame = 8;

    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.currentFrame = 0;
    }
}
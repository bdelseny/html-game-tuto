import { Brick } from "./brick.js";

export class Level {
    /** @type {Brick[][]} */
    bricks = [];

    /**
     * A level
     * @param {HTMLCanvasElement} canvas
     */
    constructor(canvas) {
        this.rows = ((canvas.height / 2) - (Brick.height * 2)) / Brick.height;
        this.columns = (canvas.width - (Brick.width * 2)) / Brick.width;
        let y = (Brick.height*2);
        for (let i = 0; i < this.rows; i++) {
            let x = Brick.width;
            this.bricks[i] = [];
            for (let j = 0; j < this.columns; j++) {
                this.bricks[i][j] = new Brick(x, y);
                x += Brick.width;
            }
            y += Brick.height;
        }
    }

    draw(ctx) {
        this.bricks.forEach(row => {
            row.forEach(brick => {
                if (brick.show) {
                    brick.draw(ctx);
                }
            })
        });
    }
}
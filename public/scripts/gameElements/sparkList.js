import { Spark } from "./spark.js";

export class SparkList {

    constructor() {
        this.spriteImage = new Image();
        this.spriteImage.src = Spark.spritePath;
        this.sparks = [];
        this.scale = 0.5;
    }

    draw(ctx) {
        let toRemove = [];
        this.sparks.forEach(spark => {
            spark.currentFrame++;
            if (spark.currentFrame > Spark.maxFrame) {
                toRemove.push(spark);
            } else {
                ctx.drawImage(this.spriteImage, (spark.currentFrame * Spark.frameWidth), 0, Spark.frameWidth, Spark.frameHeight,
                    spark.x, spark.y, Spark.frameWidth * this.scale, Spark.frameHeight * this.scale);
            }
        });
        toRemove.forEach(element => {
            this.sparks.splice(this.sparks.indexOf(element), 1);
        });
    }
}
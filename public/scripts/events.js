
export function keyDownHandler(event, paddle) {
        if(event.key == 'Right' || event.key == 'ArrowRight') {
            paddle.rightPressed = true;
        }
        else if(event.key == 'Left' || event.key == 'ArrowLeft') {
            paddle.leftPressed = true;
        }
    }

export function keyUpHandler(event, paddle) {
        if(event.key == 'Right' || event.key == 'ArrowRight') {
            paddle.rightPressed = false;
        }
        else if(event.key == 'Left' || event.key == 'ArrowLeft') {
            paddle.leftPressed = false;
        }
    }
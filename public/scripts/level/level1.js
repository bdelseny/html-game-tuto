import { Level } from "../gameElements/level.js";

export class Level1 extends Level {

    /**
     * First level
     * @param {HTMLCanvasElement} canvas
     */
    constructor(canvas) {
        super(canvas);
        for (let i = 0; i < (this.rows/2); i++) {
            for (let j = 0; j < this.columns; j++) {
                this.bricks[i][j].show = true;
                this.bricks[i][j].color = 'blue';
            }
        }
    }
}
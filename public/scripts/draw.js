import { Ball } from "./gameElements/ball.js";
import { Level } from "./gameElements/level.js";
import { Paddle } from "./gameElements/paddle.js";
import { SparkList } from "./gameElements/sparkList.js";

/**
 * @property {HTMLCanvasElement} canvas
 */
export class Draw {

    /**
     * Draw in canvas
     * @param {HTMLCanvasElement} canvas 
     * @param {Paddle} paddle 
     * @param {Ball} ball 
     * @param {Level} level 
     * @param {SparkList} sparkList 
     */
    constructor(canvas, paddle, ball, level, sparkList) {
        /** @type {HTMLCanvasElement} */
        this.canvas = canvas;
        /** @type {CanvasRenderingContext2D} */
        this.ctx = this.canvas.getContext('2d');
        /** @type {Paddle} */
        this.paddle = paddle;
        /** @type {Ball} */
        this.ball = ball;
        /** @type {Level} */
        this.level = level;
        /** @type {SparkList} */
        this.sparkList = sparkList;
    }

    async countDown() {
        this.draw();
        const sleep = ms => new Promise(r => setTimeout(r, ms));
        for (let i = 3; i > 0; i--) {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.ctx.beginPath();
            this.ctx.fillStyle = 'green';
            this.ctx.font = '50px sans-serif';
            this.ctx.textAlign = 'center';
            this.ctx.fillText(`Start in: ${i}`, this.canvas.width / 2, this.canvas.height / 2);
            this.ctx.fill();
            this.ctx.closePath();
            await sleep(1000);
        }
    }

    draw() {
        this.ctx.font = '20px sans-serif';
        this.ctx.textAlign = 'right';
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ball.draw(this.ctx);
        this.paddle.draw(this.ctx);
        this.level.draw(this.ctx);
        this.addScore(950, 20, 'Score: ');
        this.sparkList.draw(this.ctx);
        this.ball.move(this.paddle, this.level);
        this.paddle.move();
    }

    gameOver() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.beginPath();
        this.ctx.fillStyle = 'red';
        this.ctx.font = '50px sans-serif';
        this.ctx.textAlign = 'center';
        this.ctx.fillText('GAME OVER!', this.canvas.width / 2, this.canvas.height / 2);
        this.ctx.fill();
        this.ctx.closePath();
        this.addScore(this.canvas.width / 2, (this.canvas.height / 2) + 50, 'Your score is: ');
    }

    win() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.beginPath();
        this.ctx.fillStyle = 'green';
        this.ctx.font = '50px sans-serif';
        this.ctx.textAlign = 'center';
        this.ctx.fillText('YOU WIN!', this.canvas.width / 2, this.canvas.height / 2);
        this.ctx.fill();
        this.ctx.closePath();
        this.addScore(this.canvas.width / 2, (this.canvas.height / 2) + 50, 'Your score is: ');
    }

    addScore(x, y, prefix) {
        this.ctx.beginPath();
        this.ctx.fillStyle = 'blue';
        this.ctx.fillText(`${prefix || ''}${globalThis.score}`, x, y);
        this.ctx.fill();
        this.ctx.closePath();
    }
}